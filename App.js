/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppState,
  Platform,
  StyleSheet,
  PushNotificationIOS,
  Text,
  View,
  Modal
} from 'react-native';
import OneSignal from 'react-native-onesignal';
import { Toast, Root, Button, Spinner, Form, Item, Input } from "native-base";
var PushNotification = require('react-native-push-notification');

const instructions = 'Notifikasi Di foreground atau di layar terbuka berupa toast\n\n untuk mentriggernya dengan cara membuka aplikasi yg berada di kondisi tertutup \n\n\n\n\n\n notifikasi di background atau di layar tertutup berupa notifikasi dari one signal \n\n untuk mentriggernya klik button home\n\n';
const instructionsPushNotif = 'Notifikasi Di foreground atau di layar terbuka berupa notifikasi dari push notification\n\n untuk mentriggernya dengan cara membuka aplikasi yg berada di kondisi tertutup \n\n\n\n\n\n notifikasi di background atau di layar tertutup berupa notifikasi dari push notification \n\n untuk mentriggernya klik button home\n\n';

var dateFormat = require('dateformat');
let date = new Date();
var dateFormatted = dateFormat(date, "yyyy-mm-dd");

export default class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      deviceId: '',
      deviceIdOrang: '',
      message: '',
      visibleModal: false,
      page: 'OneSignal',
      isSpinner: true,
      dueDateState: [],
    }
    // this.fetchInvoice();
  }

  componentDidMount() {
    OneSignal.configure({});
    OneSignal.addEventListener('ids', this.onIds);
    AppState.addEventListener('change', this._handleAppStateChange);

  }

  fetchInvoice() {
    fetch("https://gogo6036.zahironline.com/api/sales_invoice?", {
      method: "GET",
      headers: {
        api_key: '8Tds0CHe3C8BocETfoj2EjRIbEaqkMkV8LUHfmy4',
      }
    })
      .then(response => {
        response.json().then(data => {
          let dueDates = [];
          for (let i = 0; i < data.length; i++) {
            if (data[i].term_of_payment.due_date >= dateFormatted) {
              dueDates.push({
                due_date: data[i].term_of_payment.due_date,
                invoice_number: data[i].invoice_number
              });
            }
          }
          alert('ready to comply')
          this.setState({ isSpinner: false, dueDateState: dueDates })
        });
      })
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('ids', this.onIds);
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = (nextAppState) => {
    console.log(nextAppState)
    if (nextAppState == 'background' && this.state.page == 'OneSignal') {
      this.sendNotification(this.state.deviceId, "One Signal Notification di Background Aplikasi");
    } else if (nextAppState == 'active' && this.state.page == 'OneSignal') {
      Toast.show({
        text: "One Signal Notification di Foreground Aplikasi",
        position: "top",
        duration: 3000,
      })
    } else if (nextAppState == 'background' && this.state.page == 'PushNotif') {
      this.sendPushNotif('Push Notification di Background Aplikasi')
    } else if (nextAppState == 'active' && this.state.page == 'PushNotif') {
      this.sendPushNotif('Push Notification di Foreground Aplikasi')
    }
  }

  onIds = (device) => {
    // alert(device.userId)
    this.setState({ deviceId: device.userId })
  }

  sendNotification(deviceId, message) {
    // for (let i = 0; i < this.state.dueDateState.length; i++) {

    //   let d = new Date(this.state.dueDateState[i].due_date)
    //   d.setTime(Date.now())

      fetch('https://onesignal.com/api/v1/notifications', {
        method: 'POST',
        dataType: 'json',
        headers: {
          "Content-Type": "application/json; charset=utf-8",
          "Authorization": "Basic ZjgzNjM0NGQtMDQxMC00YmI5LTllYjAtN2E5NTUxMTZhOTQ1"
        },
        body: JSON.stringify({
          "app_id": "3f086164-3a8f-4120-9123-facbdbfcffa4",
          "include_player_ids": [deviceId],
          "data": { "foo": "bar" },
          // "send_after": d,
          "template_id": '5cc1c9a7-99eb-45ab-be97-9702ddd77a9b',
          "contents": { "en": message },
          "headings": { "en": "Title Notification One Signal" },
          // "headings": { "en": this.state.dueDateState[i].invoice_number },
        })
      }).then(response => {
        response.json().then(data => {
          console.log('BETUL')
        });
      })
    // }
  }

  sendPushNotif(msg) {
    PushNotification.localNotificationSchedule({
      title: "Title Push Notification",
      message: msg, // (required)
      date: new Date(Date.now() + (2 * 1000)) // in 60 secs
    });
  }

  configurePushNotif() {
    // PushNotification.configure({
    //   onRegister: function (token) {
    //     console.log('TOKEN:', token);
    //   },
    //   onNotification: function (notification) {
    //     console.log('NOTIFICATION:', notification);
    //     notification.finish(PushNotificationIOS.FetchResult.NoData);
    //   },
    //   permissions: {
    //     alert: true,
    //     badge: true,
    //     sound: true
    //   },
    //   popInitialNotification: true,
    //   requestPermissions: true,
    // });
  }

  render() {
    return (
      <Root>
        <View style={styles.container}>
          {/* {this.state.isSpinner ? <Spinner animating={true} size={'large'} /> : null} */}
          <Text style={styles.welcome}>
            Welcome to ZO Notification Testing! Untuk One Signal
          </Text>
          
          <Form>
            <Item>
              <Input placeholder="DeviceId" onChangeText={(text) => this.setState({ deviceIdOrang: text })} />
            </Item>
            <Item last>
              <Input placeholder="Message" onChangeText={(text) => this.setState({ message: text })} />
            </Item>
          </Form>

          <Button style={{ marginVertical: 20 }} block onPress={() => this.sendNotification(this.state.deviceIdOrang, this.state.message)}>
            <Text style={{ color: 'white' }}>Send Message</Text>
          </Button>

          <Button block success onPress={() => this.setState({ visibleModal: true, page: 'PushNotif' })}>
            <Text style={{ color: 'white' }}>Go To Push Notification Package</Text>
          </Button>
        </View>





        <Modal
          visible={this.state.visibleModal}
          animationType="slide"
          onRequestClose={() => alert('wow')}
        >
          <View style={styles.container}>
            <Text style={styles.welcome}>
              Welcome to ZO Notification Testing! Untuk Push Notification
              </Text>
            <Text style={styles.instructions}>
              {instructionsPushNotif}
            </Text>

            <Button block success onPress={() => this.setState({ visibleModal: false, page: 'OneSignal' })} >
              <Text>Go To One Signal Package</Text>
            </Button>

          </View>
        </Modal>
      </Root>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
